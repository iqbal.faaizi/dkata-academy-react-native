import ACTION_TYPES from './ActionTypes.js';

export const fetchData = () => ({
  type: ACTION_TYPES.PET_PENDING
})

export const fetchSuccess = data => ({
  type: ACTION_TYPES.PET_SUCCESS,
  payload: data
})

export const fetchError = error => ({
  type: ACTION_TYPES.PET_ERROR,
  payload: error
})