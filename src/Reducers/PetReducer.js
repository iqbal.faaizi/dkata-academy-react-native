import { combineReducers } from 'redux'
import ACTION_TYPES from '../Actions/ActionTypes';

const initialState = {
  data: '',
  error: ''
};

const apiReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.PET_PENDING:
      return {
        ...state
      }
    case ACTION_TYPES.PET_SUCCESS:
      return {
        ...state,
        data: action.payload
      }
    case ACTION_TYPES.PET_ERROR:
      return {
        ...state,
        error: action.payload
      }

    default:
      return state;
  }
}

const appReducers = combineReducers({ apiReducer })
const rootReducer = (state, action) => appReducers(state, action);

export default rootReducer;