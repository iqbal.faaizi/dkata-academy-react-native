import React, { Component } from 'react'
import { StatusBar, ActivityIndicator, Text, View, Alert, ScrollView, TouchableHighlight  } from 'react-native'
import { ListItem, Card } from 'react-native-elements'
import axios from 'axios'
import styles from '../assets/styles/appStyles'
import Loading from 'react-native-whc-loading'

export function Capitalize (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export default class PetsApi extends Component {
    static navigationOptions = {
        /* title: 'PETS API HOME',
        headerStyle: { backgroundColor: '#046542' },
        headerTintColor: '#fff', */
        header: null
    }

    constructor(props){
        super(props);
        this.state ={ pets: [], isLoading: true }
    }

    async componentDidMount(){
        try {
            const response = await axios.get('http://192.168.56.1:3333/pets/')
            const responseJson = await response.data
            this.setState({
                pets: responseJson,
                isLoading: false,
            })
        } catch (err) {
            console.error(error)
        }
    }
    
    render(){
        const { isLoading, pets } = this.state
        const avatar_url= 'https://www.bing.com/th?id=OIP.ix9GSsoNSoVcm1cjcD6yqgHaHa&pid=Api&rs=1'
        const { navigate } = this.props.navigation

        if(isLoading){
            return (
                <>
                <View onLayout={() => { 
                    this.refs.loading2.show() 
                }} />
                <Loading show={true} ref='loading2' image={require('../assets/loading.png')}/>
            </>
            )
            
        }

        return(
            <>
            <StatusBar backgroundColor="#046542" />
            
            <View style={styles.apiContainer}>
                <ScrollView>
                <View style={{flexDirection:'row', justifyContent: 'center'}}>
                    <View>
                        <Text>PET LIST</Text>
                    </View>
                    <View >
                        <Text style={{textAlign: 'right'}}>PET LIST</Text>
                    </View>
                </View>

                {
                    pets.map((p, x) => (
                    <ListItem
                        key={x}
                        leftAvatar={{ source: { uri: avatar_url } }}
                        title={Capitalize(p.name)}
                        bottomDivider
                        subtitle={
                            <>
                            <View style={styles.apiListContainer}>
                                <View style={{flex: 1}}>
                                    <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Breed</Text>
                                </View>
                                <View style={{flex: 2}}>
                                    <Text style={styles.apiSubtitleList}>{Capitalize(p.breed)}</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Age</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={styles.apiSubtitleList}>{p.age}</Text>
                                </View>
                            </View>

                            <View style={styles.apiListContainer}>
                                <View style={{flex: 1}}>
                                    <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Colour</Text>
                                </View>
                                <View style={{flex: 2}}>
                                    <Text style={styles.apiSubtitleList}>{Capitalize(p.colour)}</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Next Checkup</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={styles.apiSubtitleList}>{ p.next_checkup.substring(0, 10) }</Text>
                                </View>
                            </View>
                            </>
                        }
                        
                        onPress={() => navigate('PetData', {
                            id: p.id,
                            name: p.name,
                            breed: p.breed,
                            age: p.age,
                            colour: p.colour,
                            next_checkup: p.next_checkup,
                            vaccinations: p.vaccinations
                        })}
                    />
                    ))
                }
                </ScrollView>
            </View>
            </>
        )
    }
}