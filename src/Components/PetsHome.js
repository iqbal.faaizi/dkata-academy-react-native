import React, { Component } from 'react'
import { StatusBar, ActivityIndicator, Text, View, Alert, ScrollView, TouchableHighlight, FlatList } from 'react-native'
import { ListItem, Card } from 'react-native-elements'
import { connect } from 'react-redux'
import apiFetch from '../Actions/ActionCreator'
import Loading from 'react-native-whc-loading'
import styles from '../assets/styles/appStyles'

export function Capitalize (str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

class PetsHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      avatar_url : 'https://www.bing.com/th?id=OIP.ix9GSsoNSoVcm1cjcD6yqgHaHa&pid=Api&rs=1'
    }
  }

  async componentDidMount() {
    const { apiFetch } = this.props
    try {
      const response = await apiFetch('http://192.168.56.1:3333/pets/')
      const responseJson = await response.data
      this.setState({
        data : responseJson,
      })
    } catch (err) {
      console.error(err)
    }
  }

  keyExtractor = (item, index) => index.toString()

  renderItem = ({ item }) => (
    <ListItem
      title={Capitalize(item.name)}
      subtitle={
        <>
          <View style={styles.apiListContainer}>
              <View style={{flex: 1}}>
                  <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Breed</Text>
              </View>
              <View style={{flex: 2}}>
                  <Text style={styles.apiSubtitleList}>{Capitalize(item.breed)}</Text>
              </View>
              <View style={{flex: 1}}>
                  <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Age</Text>
              </View>
              <View style={{flex: 1}}>
                  <Text style={styles.apiSubtitleList}>{item.age}</Text>
              </View>
          </View>

          <View style={styles.apiListContainer}>
              <View style={{flex: 1}}>
                  <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Colour</Text>
              </View>
              <View style={{flex: 2}}>
                  <Text style={styles.apiSubtitleList}>{Capitalize(item.colour)}</Text>
              </View>
              <View style={{flex: 1}}>
                  <Text style={[styles.apiSubtitleList, styles.apiSubtitleTitle]}>Next Checkup</Text>
              </View>
              <View style={{flex: 1}}>
                  <Text style={styles.apiSubtitleList}>{ item.next_checkup.substring(0, 10) }</Text>
              </View>
          </View>
        </>
      }
      leftAvatar={{ source: { uri: this.state.avatar_url } }}
      bottomDivider
      chevron
    />
  )

  render() {
    const { data } = this.props
    
    /* if(isLoading == true){
      return (
        <>
          <View onLayout={() => { 
              this.refs.loading2.show() 
          }} />
          <Loading ref='loading2' image={require('../assets/loading.png')}/>
      </>
      )  
    } */

    return (
      <>
        <StatusBar backgroundColor="#046542" />
        
        <View style={{flexDirection:'row', justifyContent: 'center'}}>
            <View>
                <Text>PET LIST</Text>
            </View>
            <View >
                <Text style={{textAlign: 'right'}}>PET LIST</Text>
            </View>
        </View>

        <ScrollView>
        <FlatList
          keyExtractor={this.keyExtractor}
          data={data}
          renderItem={this.renderItem}
        />
        </ScrollView>
      </>


    )
  }
}

const mapDispatchToProps = dispatch => ({
  apiFetch: url => dispatch(apiFetch(url))
});

const mapStateToProps = state => ({
  data: state.apiReducer.data,
  error: state.apiReducer.error,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PetsHome)