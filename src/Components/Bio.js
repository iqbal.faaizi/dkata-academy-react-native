import React, { Component } from 'react'
import { 
    View, Text, 
    StatusBar, 
    Image,
    TouchableOpacity, 
    TextInput,
    ScrollView, Alert
} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { Card } from 'react-native-elements'
import DatePicker from 'react-native-datepicker'
import styles from '../assets/styles/appStyles'
import axios from 'axios'

export default class Bio extends Component {
    static navigationOptions = {
        title: 'Pet Data',
        headerStyle: { backgroundColor: '#fff' },
        headerTintColor: '#000',
    }

    constructor(props){
        super(props);
        const { getParam } = this.props.navigation
        this.state = {
            id: getParam('id'),
            filePath: {},
            petData: {
                name: getParam('name'), 
                age: getParam('age'),  
                breed: getParam('breed'), 
                colour: getParam('colour'),
                next_checkup: getParam('next_checkup'),
                vaccinations: getParam('vaccinations')
            },
        }
    }

    chooseFile = () => {
        var options = {
          title: 'Select Image',
          customButtons: [
            { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);
        
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = response;
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    filePath: source,
                });
            }
        });
    }

    async updateAge(age) {
        if(age == NaN || age == null) {
            this.setState({ petData: {...this.state.petData, age: 0}})
        } else {
            this.setState({ petData: {...this.state.petData, age: age}})
        }
    }

    render () {
        return (
        <>
            <StatusBar backgroundColor="#212121" />
            <ScrollView>
            <View style={styles.container}>
                
                <View style={styles.topCont}>
                
                    <View style={ styles.topCard }>

                        <View style={{flexDirection: 'row', top: -50}}>
                            <View style={{flex:1}} />
                            <View style={{flex:1}}>
                                <TouchableOpacity style={{ marginRight: 20 }}
                                    onPress={this.chooseFile.bind(this)}
                                >
                                    <Text style={{textAlign: 'right', fontSize: 12, color:'#046542', opacity: .60}}>Change pet's photo</Text>
                                </TouchableOpacity> 
                            </View>
                        </View>

                        <Text style={{alignSelf: 'center', fontSize: 26, marginTop: 30 }}>
                            { this.state.petData.name }
                        </Text>

                        <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 10,}}>
                                <Text style={{marginRight: 20, fontSize: 18, fontWeight: 'bold', color: 'gray'}}>Breed</Text>

                                <Text style={{fontSize: 18, color: 'gray'}}>{this.state.petData.breed}</Text>
                        </View>
                    </View>
                    <View style={styles.imgCont}>
                        <Image source={require('../assets/chimpanze1.jpg')}
                            style={ styles.img }
                        />
                    </View>
                </View>
                
                {/* Bottom Container */}
                <View style={ styles.btmContainer }>
                    
                    <View style={ styles.btmBar }>
                        <View style={styles.container}>
                            <Text style={styles.txtFont}>Age</Text>
                        </View>
                        <View style={styles.container}>
                            <TextInput
                                style={ styles.txtInput }
                                value={(this.state.petData.age)}
                                keyboardType='number-pad'
                                onChangeText={age => this.updateAge(age)}
                            />
                        </View>
                    </View>
                    <View style={ styles.btmBar }>
                        <View style={styles.container}>
                            <Text style={styles.txtFont}>Color</Text>
                        </View>
                        <View style={styles.container}>
                            <TextInput
                                style={ styles.txtInput }
                                value={ this.state.petData.colour }
                                onChangeText={text => this.setState({
                                    petData: {...this.state.petData, colour: text}
                                })}
                            />
                        </View>
                    </View>

                    <View style={ styles.btmBar }>
                        <View style={styles.container}>
                            <Text style={styles.txtFont}>Next Checkup</Text>
                        </View>
                        <View style={styles.container}>
                            <DatePicker
                                style={{width: 200}}
                                date={ this.state.petData.next_checkup }
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate="2012-01-01"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel" 
                                customStyles={{
                                    dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                    },
                                    dateInput: {
                                    marginLeft: 36
                                    }
                                }}
                                onDateChange={(date) => {this.setState({petData: {...this.state.petData, next_checkup: date}})}}
                            />
                        </View>
                    </View>

                    <Card title='VACCINATIONS'
                        containerStyle={{margin: 0}}
                    >
                        <View style={{flexDirection: 'column'}}>
                            <Text>{this.state.petData.vaccinations[0]}</Text>
                            <Text>{this.state.petData.vaccinations[1]}</Text>
                            <Text>{this.state.petData.vaccinations[2]}</Text>
                            <Text>{this.state.petData.vaccinations[3]}</Text>
                        </View>
                    </Card>
                </View>
                
            </View>
            </ScrollView>


            <TouchableOpacity 
                style={{height: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: '#046542'}}
                onPress={() => Alert.alert(`You can't buy a pet! But you can have this cute one ^_^. Say hi to ${this.state.petData.name}`)}
            >
                <Text style={{fontSize: 22, color: '#fff'}}>Order</Text>
            </TouchableOpacity>
        </>
        )
    }

    componentWillUnmount() {
        console.log(this.state.petData)
        axios.put(`http://192.168.56.1:3333/pets/${this.state.id}`, 
        this.state.petData,{})
    }
}