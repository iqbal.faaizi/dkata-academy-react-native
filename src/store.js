import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import rootReducer from './Reducers/petReducer';

const middleware = [thunk];

if (__DEV__) {
  middleware.push(createLogger())
}

export default createStore(
  rootReducer,
  compose(applyMiddleware(...middleware))
)