import React from 'react'
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    apiContainer: {
        flex: 1, 
        backgroundColor: '#f1f1f1'
    },
    apiListContainer:{
        flexDirection:'row', 
        marginTop: 5
    },
    apiSubtitleList: {
        color: 'gray'
    },
    apiSubtitleTitle: {
        fontWeight: 'bold'
    },
    container: {
        flex: 1,
    },
    topCont: {
        flex: 1,
        backgroundColor: '#09D38A'
    },
    topCard: {
        backgroundColor: '#fff',
        margin: 20, 
        height: 250, 
        top: 50, 
        borderRadius: 20, 
        justifyContent: 'center',
    },
    img: {
        height: 150,
        width: 150,
        borderRadius: 150,
        alignSelf: 'center',
        top: -270,
        marginBottom: -100
    },
    btmContainer: {
        flex: 1,
        justifyContent: 'center', 
        paddingHorizontal: 30,
        paddingVertical: 35,
        backgroundColor: '#f1f1f1',
    },
    btmBar: {
        flexDirection: 'row',
        marginBottom: 20
    },
    txtFont: {
        fontSize: 18,
        height: 50,
        paddingTop: 10
    },
    txtInput: {
        borderBottomWidth: 1,
        fontSize: 18,
        borderBottomColor: '#046542',
        color: '#046542',
        opacity: .70
    },
    txtName: {
        fontSize: 20,
        paddingTop: 10,
        color: '#3b7636'
    }
})

export default styles