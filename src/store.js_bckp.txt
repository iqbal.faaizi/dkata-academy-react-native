/* import rootReducer from './reducers'
import { applyMiddleware, createStore, compose } from 'redux'
//import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'

const middlewares = [thunk];
if (__DEV__) {
    middlewares.push(createLogger())
}

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(...middlewares)
    )
)

export default store */


import { createStore, combineReducers } from 'redux'
import placeReducer from './Reducers/placeReducer'

const rootReducer = combineReducers({
    places: placeReducer
})

const configureStore = () => {
    return createStore(rootReducer)
}

export default configureStore