import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import Bio from './src/Components/Bio'
import PetsApi from './src/Components/PetsApi'

const AppNavigator = createStackNavigator({
    Home: {screen: PetsApi},
    PetData: {screen: Bio}
},{initialRouteName: 'Home'})

export default createAppContainer(AppNavigator)